<?php
return [
    /*
     |--------------------------------------------------------------------------
     | Facebook
     |--------------------------------------------------------------------------
     |
     | https://developers.facebook.com/tools/explorer/145634995501895/?method=GET&path=awakenings%2Fevents&version=v2.12
     |
     */
    'fb_token' => env('FB_TOKEN', ''),
];

