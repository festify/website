<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::prefix('v1')->namespace('api')->get('/events/{id}', 'EventController@index');

Route::prefix('v1')->namespace('api')->group(function () {
    Route::resource('events', 'EventController');
    Route::resource('genres', 'GenreController');
    Route::resource('venues', 'VenueController');

//    Route::resource('events', function () {
//        dd(1);
//    });
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
