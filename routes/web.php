<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/welcome', 'WelcomeController@index')->name('login');

//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

//Crawler
Route::get('/crawler/events', 'Crawlers\EventController@index');

//Pages
Route::get('/concepts/{slug}', 'ConceptController@show');
Route::get('/events/{slug}/lineup', 'EventController@lineup');
Route::get('/events/{slug}', 'EventController@show');