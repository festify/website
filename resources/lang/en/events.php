<?php

return [

    'genre' => 'Genre',
    'about' => 'About',
    'description' => 'Description',
    'dates' => 'Dates',
    'date' => 'Date',
    'first edition' => 'First edition',

];
