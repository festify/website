@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row spotlight">
            <div class="container">
                <div class="col-lg-12">
                    <div class="spotlight-background" style="background-image: url('/{{ $logo->path }}{{ $logo->original_name }}');">
                        <div class="spotlight-info">
                            <div class="spotlight-logo">
                                <img src="/{{ $spotlight->path }}{{ $spotlight->original_name }}" title="Awakenings logo" alt="Awakenings logo" />
                            </div>
                            <h1>{{ $event->name }}</h1>
                            <h2>Techno</h2>
                            <div class="row spotlight-count">
                                <div class="col-lg-6 spotlight-count-box">
                                    <div class="spotlight-count-number">
                                        10.000.000
                                    </div>
                                    <div class="spotlight-count-name">
                                        Likes
                                    </div>
                                </div>
                                <div class="col-lg-6 spotlight-count-box">
                                    <div class="spotlight-count-number">
                                        10.000.000
                                    </div>
                                    <div class="spotlight-count-name">
                                        Followers
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <nav class="navbar navbar-entity navbar-events">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#" class="active">Latest</a>
                </li>
                <li>
                    <a href="#">Photos</a>
                </li>
                <li>
                    <a href="#">Events</a>
                </li>
            </ul>
        </nav>

    <div class="container-fluid">
        <div class="row main">
            <div class="container">
                <div class="col-lg-3 rightbar">
                    <div class="card">
                        <h4>About</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="list-item-title">First edition</div>
                                <div class="list-item-content">1997</div>
                            </li>
                            <li class="list-group-item">
                                <div class="list-item-title">Cras justo odio</div>
                                <div class="list-item-content">Dapibus ac facilisis in</div>
                            </li>
                            <li class="list-group-item">
                                <div class="list-item-title">Cras justo odio</div>
                                <div class="list-item-content">Dapibus ac facilisis in</div>
                            </li>
                            <li class="list-group-item">
                                <div class="list-item-title">Cras justo odio</div>
                                <div class="list-item-content">Dapibus ac facilisis in</div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <h4>Description</h4>
                        {!! $event->description !!}
                        <small>Bron: Wikipedia</small>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card">
                        <h4>About</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
