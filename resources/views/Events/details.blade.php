@extends('events.layout')

@section('page_content')
<div class="col-lg-3 rightbar">
    <div class="box">
        <h4>{{ trans('events.about') }}</h4>
        <ul class="list-group">
            <li class="list-group-item">
                <h6 class="list-item-title">{{ trans('events.genre') }}</h6>
                <div class="list-item-content">Techno</div>
            </li>
            <li class="list-group-item">
                <h6 class="list-item-title">{{ trans('events.first edition') }}</h6>
                <div class="list-item-content">1997</div>
            </li>
            <li class="list-group-item">
                <h6 class="list-item-title">
                    {{--@if (count($event->dates) >= 2)--}}
                        {{--{{ trans('events.dates') }}--}}
                    {{--@else--}}
                        {{--{{ trans('events.date') }}--}}
                    {{--@endif--}}
                </h6>
                <div class="list-item-content">
                    {{--@forelse($event->dates as $date)--}}
                        {{--<p>{{ $date->start_at->formatLocalized('%A %d %B %Y') }}</p>--}}
                    {{--@empty--}}
                        {{--<p>T.B.A.</p>--}}
                    {{--@endforelse--}}
                </div>
            </li>
            <li class="list-group-item">
                <h6 class="list-item-title">Cras justo odio</h6>
                <div class="list-item-content">Dapibus ac facilisis in</div>
            </li>
            <li class="list-group-item">
                <h6 class="list-item-title">Cras justo odio</h6>
                <div class="list-item-content">Dapibus ac facilisis in</div>
            </li>
        </ul>
    </div>
</div>

<div class="col-lg-6">
    <div class="box">
        <h4>{{ trans('events.description') }}</h4>
        {!! $event->description !!}
        <small>Bron: Wikipedia</small>
    </div>
</div>

<div class="col-lg-3">
    <div class="box">
        <h4>{{ trans('events.about') }}</h4>
    </div>
</div>
@endsection
