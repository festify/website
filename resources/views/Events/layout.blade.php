@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="container spotlight">
        <div class="row">
            <div class="col-lg-12">
                <div class="spotlight-background" style="background-image: url('{{ $spotlight->path }}');">
                    <div class="spotlight-info">
                        <div class="spotlight-logo">
                            <img class="img-fluid" src="{{ $logo->path }}" title="{{ $event->name }}" alt="{{ $event->name }}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container name">
    <div class="row">
        <div class="col-lg-12">
            <h1>{{ $event->name }}</h1>
        </div>
    </div>
</div>

<div class="container navbar-entity navbar-events">
        <div class="navbar navbar-expand-lg">
            <nav class="collapse navbar-collapse justify-content-md-center">
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a href="{!! $event->path !!}" class="btn">
                            Latest <!--<span class="badge badge-secondary">9</span>-->
                            <span class="sr-only">unread messages</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{!! $event->lineup_path !!}" class="btn">
                            Lineup <!--<span class="badge badge-secondary">9</span>-->
                            <span class="sr-only">unread messages</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="btn">
                            Photos <!--<span class="badge badge-secondary">9</span>-->
                            <span class="sr-only">unread messages</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
</div>

<div class="container-fluid">
    <div class="container main">
        <div class="row">
            @yield('page_content')
        </div>
    </div>
</div>
@endsection
