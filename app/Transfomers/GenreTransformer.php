<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Genre;

class GenreTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    protected $availableIncludes = [];

    public function transform(Genre $genre)
    {
        $site_url = config('app.url');

        $transformed = [
            'id' => (int)$genre->id,
            'name' => $genre->name,
            'slug' => $genre->slug,
        ];

        return $transformed;
    }
}