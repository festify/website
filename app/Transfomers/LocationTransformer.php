<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Location;

class LocationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    protected $availableIncludes = [];

    public function transform(Location $locations)
    {
        $site_url = config('app.url');

        $transformed = [
            'id' => (int)$locations->id,
            'latitude' => $locations->latitude,
            'longitude' => $locations->longitude,
            'street_name' => $locations->street_name,
            'street_number' => $locations->street_number,
            'street_number_suffix' => $locations->street_number_suffix,
            'street_direction' => $locations->street_direction,
            'postal_area' => $locations->postal_area,
            'city' => $locations->city->name,
            'country' => $locations->city->country->iso_3166_3,

            'link' => $site_url . $locations->path,
            'created' => $locations->created_at->toIso8601String(),
            'updated' => $locations->updated_at->toIso8601String(),
        ];

        return $transformed;
    }

//    public function includeLocation(Location $venue)
//    {
//        dd($venue->location);
//        if (!empty($event->venue)) {
//            return $this->collection($event->venue, new VenueTransformer, 'venue');
//        }
//
//        return $this->null();
//    }
}