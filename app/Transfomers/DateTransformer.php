<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Date;

class DateTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    protected $availableIncludes = [
        'events'
    ];

    public function transform(Date $date)
    {
//        dd($date);
        $transformed = [
            'id' => (int)$date->id,
            'name' => $date->name,
            'created' => $date->created_at->toIso8601String(),
            'updated' => $date->updated_at->toIso8601String(),
            'links' => [
                [
                    'rel' => 'self',
//                    'uri' => route('events', $event->id),
                ]
            ]
        ];

//        if (!empty($event->dates)) {
//            foreach ($event->dates AS $key => $dates) {
//                $transformed_event['dates'][$key]['start_at'] = $dates->start_at->toIso8601String();
//                $transformed_event['dates'][$key]['end_at'] = $dates->end_at->toIso8601String();
//            }
//        }

        return $transformed;
    }

//    public function includeFolder(File $file)
//    {
//        if (!empty($file->folder) && $file->folder->count() != 0) {
//            return $this->collection($file->folder, new FolderTransformer, 'folder');
//        }
//        return $this->null();
//    }
}