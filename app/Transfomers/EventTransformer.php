<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Event;

class EventTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    protected $availableIncludes = [
        'venue',
        'location'
    ];

    public function transform(Event $event)
    {
        $site_url = config('app.url');

        $transformed = [
            'id' => (int)$event->id,
            'name' => $event->name,
            'slug' => $event->slug,
            'link' => $site_url . $event->path,
            'description' => $event->description,
            'logo_image' => !empty($event->logo) ? $site_url . $event->logo->path : null,
            'spotlight_image' => !empty($event->spotlight) ? $site_url . $event->spotlight->path : null,
            'created' => $event->created_at->toIso8601String(),
            'updated' => $event->updated_at->toIso8601String(),
            'dates' => null
        ];

//        dd($event->dates);

        if (!empty($event->dates)) {
            foreach ($event->dates AS $key => $dates) {
                $transformed['dates'][$key]['starts_at'] = !empty($dates->starts_at) ? $dates->starts_at->toIso8601String() : null;
                $transformed['dates'][$key]['ends_at'] = !empty($dates->ends_at) ? $dates->ends_at->toIso8601String() : null;
            }
        }

        return $transformed;
    }

    public function includeVenue(Event $event)
    {
        $venue = $event->venue;
        if (!empty($venue)) {
            return $this->item($venue, new VenueTransformer, 'venues');
        }
    }

    public function includeLocation(Event $event)
    {
        $location = $event->location;
        if (!empty($location)) {
            return $this->item($location, new LocationTransformer, 'locations');
        }
    }

//    public function includeDates(Event $event)
//    {
////        dd($event->dates);
//        if (!empty($event->dates) && $event->dates->count() != 0) {
//            return $this->collection($event->dates, new DateTransformer, 'dates');
//        }
//        return $this->null();
//    }
}