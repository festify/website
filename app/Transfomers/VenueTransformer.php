<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Venue;

class VenueTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    protected $availableIncludes = [
        'location'
    ];

    public function transform(Venue $venue)
    {
        $site_url = config('app.url');

        $transformed = [
            'id' => (int)$venue->id,
            'name' => $venue->name,
            'slug' => $venue->slug,
            'link' => $site_url . $venue->path,
            'created' => $venue->created_at->toIso8601String(),
            'updated' => $venue->updated_at->toIso8601String(),
        ];

        return $transformed;
    }

    public function includeLocation(Venue $venue)
    {
        if (!empty($venue->location)) {
            return $this->item($venue->location, new LocationTransformer, 'locations');
        }

        return $this->null();
    }
}