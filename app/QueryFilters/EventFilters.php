<?php namespace App\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Carbon\Carbon;

class EventFilters extends QueryFilters
{
    protected $implicitFilters = [
        'startsAt',
    ];

    public function startsAt($date)
    {
        $this->query->whereHas('dates', function ($query) use ($date) {
            $query->where('dates.starts_at', '=', Carbon::createFromTimestamp($date));
        });
    }

    public function endsAt($date)
    {
        $this->query->whereHas('dates', function ($query) use ($date) {
            $query->where('dates.ends_at', '=', Carbon::createFromTimestamp($date));
        });
    }

    public function startsAfter($date)
    {
        $this->query->whereHas('dates', function ($query) use ($date) {
            $query->where('dates.starts_at', '>=', Carbon::createFromTimestamp($date));
        });
    }

    public function startsBefore($date)
    {
        $this->query->whereHas('dates', function ($query) use ($date) {
            $query->where('dates.ends_at', '>=', Carbon::createFromTimestamp($date));
        });
    }

    public function name($name)
    {
        $this->query->whereTranslationLike('name', '%' . $name . '%');
    }

    public function venue($param)
    {
//        $this->query->whereHas('venue', function ($query) use ($param) {
        $this->query->where('venue_id', $param);
//        });
    }

    public function genres($genres)
    {
        $genres = explode(',', $genres);

        $this->query->whereHas('genres', function ($query) use ($genres) {
            $query->orWhereIn('genres.id', $genres);
        });
    }
}
