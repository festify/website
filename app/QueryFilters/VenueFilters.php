<?php namespace App\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Carbon\Carbon;

class VenueFilters extends QueryFilters
{
    protected $implicitFilters = [];

    public function name($name)
    {
        $this->query->whereTranslationLike('name', '%' . $name . '%');
    }
}
