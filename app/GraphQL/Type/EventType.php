<?php namespace App\GraphQL\Type;

use App\Models\Event;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class EventType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Event',
        'description' => 'A type',
        'model' => Event::class, // define model for users type
    ];

    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the event'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the event',
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of the event',
            ],
//            // field relation to model user_profiles
//            'event_translation' => [
//                'type' => GraphQL::type('EventTranslation'),
//                'description' => 'The profile of the user'
//            ]
        ];
    }

//    protected function resolveNameField($root, $args)
//    {
////        dump($args);
////        dd($root);
//        return strtolower($root->name);
//    }
}