<?php namespace App\GraphQL\Query;

use App\Models\Event;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class EventsQuery extends Query
{
    protected $attributes = [
        'name' => 'Events Query',
        'description' => 'A query of events'
    ];

    public function type()
    {
        // result of query with pagination laravel
        return Type::listOf(GraphQL::type('event'));
//        return GraphQL::paginate('user');
    }

    // arguments to filter query
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            if (isset($args['id'])) {
                $query->where('id', $args['id']);
            }
            if (isset($args['name'])) {
                $query->where('name', $args['name']);
            }
        };
        $events = Event::with(array_keys($fields->getRelations()))
            ->where($where)
//            ->select($fields->getSelect())
            ->paginate();
        return $events;
    }
}