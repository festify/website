<?php namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Country extends Eloquent
{
//    use Translatable;
//    use SoftDeletes;

//    public $translatedAttributes = [
//        'name',
//        'slug',
//        'description'
//    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
//    protected $with = [
//        'translations'
//    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
//    protected $dates = [
//        'created_at',
//        'updated_at',
//        'deleted_at'
//    ];

    /**
     * Get all of location for the venue.
     */
    public function location()
    {
        return $this->hasMany('App\Models\Location');
    }

    /**
     * Get all of location for the venue.
     */
    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }

}
