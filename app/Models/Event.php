<?php namespace App\Models;

//use App\Models\File;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Cerbero\QueryFilters\FiltersRecords;

class Event extends Eloquent
{
    use Translatable;
    use SoftDeletes;
    use FiltersRecords;

    public $translatedAttributes = [
        'name',
        'slug',
        'description'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'fb_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $filters = [
        'name' => 'like',
//        'dates.starts_at' => '',
//        'dates.ends_at'
    ];

    /**
     * Get all of venue for the event.
     */
    public function venue()
    {
        return $this->belongsTo('App\Models\Venue');
    }

    /**
     * Get all of files for the event.
     */
    public function dates()
    {
        return $this->hasMany('App\Models\Date')->orderBy('starts_at');
    }

    /**
     * Get all of files for the event.
     */
    public function files()
    {
        return $this->morphToMany(File::class, 'filable')->withPivot('type', 'order');
    }

    /**
     * Get all of files for the event.
     */
    public function folder()
    {
        return $this->belongsTo('App\Models\Folder');
    }

    /**
     * Get all of the genres for the event.
     */
    public function genres()
    {
        return $this->morphToMany(Genre::class, 'genrable');
    }

//    /**
//     * Get all of files for the event.
//     */
//    public function images()
//    {
//        dd($this->files);
//    }

//    public function getActiveFilesAttribute()
//    {
//        $files = [];
//        $files['logo'] = $this->activeLogo;
//        $files['spotlight'] = $this->activeSpotlight;
//
//        return $files;
//    }

    public function getLogoAttribute()
    {
        return $this->files()->where('filables.type', 'logo')->orderBy('pivot_order', 'ASC')->first();
    }

    public function getSpotlightAttribute()
    {
        return $this->files()->where('filables.type', 'spotlight')->orderBy('pivot_order', 'ASC')->first();
    }

    /**
     * Get the URL of this event.
     */
    public function getPathAttribute()
    {
        return '/events/' . $this->slug;
    }

    /**
     * Get the URL of this event.
     */
    public function getLineupPathAttribute()
    {
        return $this->path . '/lineup';
    }

}
