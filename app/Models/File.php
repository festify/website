<?php namespace App\Models;

//use App\Models\Event;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class File extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Get all of files for the event.
     */
    public function events()
    {
        return $this->morphedByMany(Event::class, 'filable');
    }

    /**
     * Get the folder of the file
     */
    public function folder()
    {
        return $this->belongsTo('App\Models\Folder');
    }

    public function getPathAttribute()
    {
        return '/'.$this->folder->path.'/'.$this->original_name;
    }

    public function getType($type)
    {
        dd($type);
    }


}
