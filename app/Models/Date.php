<?php namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Date extends Eloquent
{
    use Translatable;
    use SoftDeletes;
//    use Sluggable;

    public $translatedAttributes = [
        'name',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'starts_at',
        'ends_at'
    ];

    /**
     * Get all of files for the event.
     */
    public function events()
    {
        return $this->belongsTo('App\Models\Event');
    }

    /**
     * Set the starts at date to UTC.
     *
     * @param  string $value
     * @return void
     */
    public function setStartsAtAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['starts_at'] = $value->setTimezone('UTC');
        } else {
            $this->attributes['starts_at'] = null;
        }
    }

    /**
     * Set the ends at date to UTC.
     *
     * @param  string $value
     * @return void
     */
    public function setEndsAtAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['ends_at'] = $value->setTimezone('UTC');
        } else {
            $this->attributes['ends_at'] = null;
        }
    }

//    /**
//     * Get all of files for the event.
//     */
//    public function files()
//    {
//        return $this->morphToMany('App\Models\File', 'filable')->withPivot('type');
//    }

}
