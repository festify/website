<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class DateTranslation extends Eloquent
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

}
