<?php namespace App\Models;

//use App\Models\Event;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Performance extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'artist_id',
        'event_id',
        'date_id',
        'starts_at',
        'ends_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'starts_at',
        'ends_at'
    ];

    /**
     * Get all of files for the event.
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * Get the folder of the file
     */
    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    /**
     * Get the folder of the file
     */
    public function date()
    {
        return $this->belongsTo(Date::class);
    }

    /**
     * Set the starts at date to UTC.
     *
     * @param  string  $value
     * @return void
     */
    public function setStartsAtAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['starts_at'] = $value->setTimezone('UTC');
        } else {
            $this->attributes['starts_at'] = null;
        }
    }

    /**
     * Set the ends at date to UTC.
     *
     * @param  string  $value
     * @return void
     */
    public function setEndsAtAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['ends_at'] = $value->setTimezone('UTC');
        } else {
            $this->attributes['ends_at'] = null;
        }
    }

//    public function getPathAttribute()
//    {
//        return '/'.$this->folder->path.'/'.$this->original_name;
//    }
//
//    public function getType($type)
//    {
//        dd($type);
//    }
}
