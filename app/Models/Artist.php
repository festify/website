<?php namespace App\Models;

//use App\Models\File;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Artist extends Eloquent
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'name',
        'slug',
        'description',
        'biography',
        'website'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'birth_date',
        'fb_event_page',
        'fb_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'birth_date'
    ];

    /**
     * Get all of performances for the artist.
     */
    public function performances()
    {
        return $this->hasMany(Performance::class)->orderBy('starts_at');
    }

    /**
     * Get all of files for the event.
     */
    public function files()
    {
        return $this->morphToMany(File::class, 'filable')->withPivot('type', 'order');
    }

    /**
     * Get all of files for the event.
     */
    public function folder()
    {
        return $this->belongsTo('App\Models\Folder');
    }

    /**
     * Get all of the genres for the artist.
     */
    public function genres()
    {
        return $this->morphToMany(Genre::class, 'genrable');
    }

//    public function setNameAttribute()
//    {
//        return $this->files()->where('filables.type', 'logo')->orderBy('pivot_order', 'ASC')->first();
//    }

    //TODO: Change to picture
//    public function getLogoAttribute()
//    {
//        return $this->files()->where('filables.type', 'logo')->orderBy('pivot_order', 'ASC')->first();
//    }
//
//    public function getSpotlightAttribute()
//    {
//        return $this->files()->where('filables.type', 'spotlight')->orderBy('pivot_order', 'ASC')->first();
//    }

}
