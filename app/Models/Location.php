<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Location extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'latitude',
        'longitude',
        'street_number',
        'street_number_suffix',
        'street_name',
        'street_direction',
        'postal_area',
        'city_id',
        'country_id',
        'fb_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Get all of location for the venue.
     */
    public function venues()
    {
        return $this->hasMany('App\Models\Venue');
    }

    /**
     * Get teh country for the location.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the city for the location.
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

}
