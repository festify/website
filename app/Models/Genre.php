<?php namespace App\Models;

//use App\Models\File;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Genre extends Eloquent
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'name',
        'slug'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Get all of the artists that are assigned this genre.
     */
    public function artists()
    {
        return $this->morphedByMany(Artist::class, 'genrable');
    }

//    /**
//     * Get all of files for the event.
//     */
//    public function events()
//    {
//        return $this->hasMany('App\Models\Date')->orderBy('starts_at');
//    }
//
//    /**
//     * Get all of files for the event.
//     */
//    public function files()
//    {
//        return $this->morphToMany(File::class, 'filable')->withPivot('type', 'order');
//    }
//
//    /**
//     * Get all of files for the event.
//     */
//    public function folder()
//    {
//        return $this->belongsTo('App\Models\Folder');
//    }

    //TODO: Change to picture
//    public function getLogoAttribute()
//    {
//        return $this->files()->where('filables.type', 'logo')->orderBy('pivot_order', 'ASC')->first();
//    }

//    public function getSpotlightAttribute()
//    {
//        return $this->files()->where('filables.type', 'spotlight')->orderBy('pivot_order', 'ASC')->first();
//    }

}
