<?php namespace App\Console\Commands\Facebook;

use Illuminate\Console\Command;
use App\Models\Event;
use App\Models\File;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SyncEventPictures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fb:sync-event-pictures';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $mimeTypes = [
        'image/jpeg' => 'jpg',
        'image/jpg' => 'jpg',
        'image/gif' => 'gif',
        'image/png' => 'png'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::whereNotNull('fb_id')->doesntHave('files')->orderBy('updated_at', 'DESC')->paginate(100);

        $client = new Client();

        foreach ($events as $event) {
            $response = $client->get('https://graph.facebook.com/v2.12/' . $event->fb_id . '/picture?access_token=' . config('crawler.fb_token') . '&debug=all&format=json&method=get&pretty=0&redirect=false&suppress_http_code=1&type=large');
            $array = json_decode($response->getBody()->getContents(), true);

            $url = $array['data']['url'];
            $contents = file_get_contents($url);

            $file_info = new \finfo(FILEINFO_MIME_TYPE);
            $mime_type = $file_info->buffer($contents);
            $extension = $this->mimeTypes[$mime_type];

            $new_filename = $event->folder->path . '/' . $event->slug . '.' . $extension;

            $is_file_stored = Storage::disk('local_public')->put($new_filename, $contents);

            $file = new File;
            $file->file_name = $event->slug;
            $file->original_name = $event->slug . '.' . $extension;
            $file->mime_type = $mime_type;
            $file->extension = $extension;
            $file->folder_id = $event->folder->id;
//            $file->type = 'logo';
            $file->save();

            $event->files()->sync(
                [$file->id => ['type' => 'logo', 'order' => 1]]
            );

            $this->info('Synced: ' . $event->name . ' (' . $event->id . ') - ' . $file->name . ' (' . $file->id . ')');
        }

//        dd($events);
    }
}
