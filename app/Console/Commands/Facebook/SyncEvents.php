<?php namespace App\Console\Commands\Facebook;

use Illuminate\Console\Command;
use App\Models\Event;
use App\Models\Date;
use App\Models\Organizer;
use App\Models\Artist;
use App\Models\Location;
use App\Models\Country;
use App\Models\Venue;
use App\Models\City;
use App\Models\Folder;
use App\Models\Performance;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SyncEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fb:sync-events {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $soldOutTerms = [
        ' (SOLD OUT)',
        ' (Sold out)',
        ' - SOLD OUT',
        ' [SOLD OUT]',
        'SOLD OUT - ',
        'Sold out - ',
        ' [Sold Out]',
        ' - sold out -',
        ' (SOLD-OUT)',
        ' - Sold out',
        'Sold out',
        'SOLD OUT'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('model') === 'organizer') {
            $pages = Organizer::whereNotNull('fb_event_page')->where('syncable', 1)->get();
        } elseif ($this->argument('model') === 'artist') {
            $pages = Artist::whereNotNull('fb_event_page')->orderBy('created_at', 'DESC')->get();
        }

        $pages = $pages->pluck('fb_event_page');

        $client = new Client();

        foreach ($pages as $key => $page) {
            $response = $client->get('https://graph.facebook.com/v2.12/' . $page . '/events?access_token=' . config('crawler.fb_token') . '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1');
            $body = json_decode($response->getBody()->getContents(), true);

            if ($this->argument('model') === 'artist') {
                $artist = Artist::where('fb_event_page', $page)->first();
            }

            foreach ($body['data'] as $event_data) {
                $eventName = trim(str_replace($this->soldOutTerms, '', $event_data['name']));

                $event = Event::where('fb_id', $event_data['id'])->first();

                //If it doesn't exist create a new event
                if (empty($event)) {
                    $event = new Event;
                    $event->fb_id = $event_data['id'];
                }

                $event->name = $eventName;
                $event->description = !empty($event_data['description']) ? $event_data['description'] : '';

                $event->save();

                $folder = $this->createFolder($event);
                $event->folder_id = $folder->id;

                if (!empty($event_data['start_time']) || !empty($event_data['end_time'])) {
                    $this->saveDates($event, $event_data);
                }

                if (!empty($event_data['place'])) {
                    $venue = $this->savePlace($event_data);

                    $event->venue_id = $venue->id;
                }

                if (!empty($artist)) {
                    $performance = new Performance;
                    $performance->artist()->associate($artist);
                    $performance->event()->associate($event);

                    $performance->save();
                }

                $event->save();

                $this->info('Synced: ' . $event->name . ' (' . $event->id . ')');
            }
        }
    }

    public function savePlace($data)
    {

        if (!empty($data['place'])) {
            $event_venue = Venue::whereTranslation('name', $data['place']['name'])->first();

            if (empty($event_venue)) {
                $event_venue = new Venue;
                $event_venue->name = !empty($data['place']['name']) ? $data['place']['name'] : null;
                $event_venue->fb_id = !empty($data['place']['id']) ? $data['place']['id'] : null;

                //Location
                if (!empty($data['place']['location'])) {
                    $event_location = Location::where('latitude', $data['place']['location']['latitude'])
                        ->where('longitude', $data['place']['location']['longitude'])
                        ->first();

                    if (empty($event_location)) {
                        if (!empty($data['place']['location']['country'])) {
                            $country = Country::where('name', 'LIKE', $data['place']['location']['country'])->first();
                        } else {
                            $country = null;
                        }

                        $event_location = new Location;
                        $event_location->street_name = !empty($data['place']['location']['street']) ? $data['place']['location']['street'] : null;
                        $event_location->postal_area = !empty($data['place']['location']['zip']) ? $data['place']['location']['zip'] : null;
                        $event_location->latitude = !empty($data['place']['location']['latitude']) ? $data['place']['location']['latitude'] : null;
                        $event_location->longitude = !empty($data['place']['location']['longitude']) ? $data['place']['location']['longitude'] : null;
                        $event_location->country_id = !empty($country) ? $country->id : null;

                        if (!empty($data['place']['location']['city'])) {
                            $event_city = $this->findOrSaveCity($data['place']['location']['city'], $country);

                            $event_location->city_id = !empty($event_city) ? $event_city->id : null;
                        }

                        $event_location->save();
                    }

                    $event_venue->location_id = $event_location->id;
                }

                $event_venue->save();
            }

            return $event_venue;
        }

        return false;
    }

    public function findOrSaveCity($city, $country = null)
    {
        $event_city = City::whereTranslation('name', $city)->first();

        if (empty($event_city)) {
            $event_city = new City;
            $event_city->name = !empty($city) ? $city : null;
            $event_city->country_id = !empty($country) ? $country->id : null;

            $event_city->save();
        }

        return $event_city;
    }

    public function saveDates($event, $data)
    {
//        if ($event->fb_id != '1706192306163055') {

            if ($event->dates->isEmpty() === true) {
                $date = new Date;
                $date->starts_at = !empty($data['start_time']) ? Carbon::createFromFormat('Y-m-d\TH:i:sO', $data['start_time']) : null;
                $date->ends_at = !empty($data['end_time']) ? Carbon::createFromFormat('Y-m-d\TH:i:sO', $data['end_time']) : null;
                $date->event_id = $event->id;

                $date->save();

                return $date;
            }

//        }

        return $event->dates;
    }

    public function createFolder($entity)
    {
        if (empty($entity->folder)) {
            $folder = new Folder;
            $folder->path = 'events/' . $entity->slug;

            $folder->save();

            return $folder;
        }

        return $entity->folder;
    }

}