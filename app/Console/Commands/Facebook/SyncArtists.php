<?php namespace App\Console\Commands\Facebook;

use Illuminate\Console\Command;
use App\Models\Genre;
use App\Models\Date;
use App\Models\Artist;
use App\Models\Location;
use App\Models\Country;
use App\Models\Venue;
use App\Models\City;
use App\Models\Folder;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\Count;

class SyncArtists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fb:sync-artists';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $artists = Artist::whereNotNull('fb_event_page')->get();

//        dd($artists);

        $pages = $artists->pluck('fb_event_page');

        $client = new Client();

        foreach ($pages as $key => $page) {
//            dump($page);
            $response = $client->get('https://graph.facebook.com/v2.12/' . $page . '?access_token=' . config('crawler.fb_token') . '&fields=about,bio,birthday,band_members,affiliation,description_html,description,general_info,genre,hometown,influences,link,name,website,id');
            $data = json_decode($response->getBody()->getContents(), true);

//            dd($body);

//            foreach ($body['data'] as $data) {
            if (!empty($page)) {
                $artist = Artist::where('fb_event_page', $page)->first();
            } else {
                $artist = Artist::where('fb_id', $data['id'])->first();
            }

            //If it doesn't exist create a new event
            if (empty($artist)) {
                $artist = new Artist;
                $artist->fb_id = $data['id'];
            }

            $artist->name = !empty($data['name']) ? $data['name'] : '';
            $artist->website = !empty($data['website']) ? $data['website'] : '';
            $artist->description = !empty($data['about']) ? $data['about'] : '';
            $artist->biography = !empty($data['bio']) ? $data['bio'] : '';
            $artist->fb_id = !empty($data['id']) ? $data['id'] : '';

//                $artist->birth_date = !empty($event_data['birthday']) ? $event_data['birthday'] : '';
            $artist->birth_date = !empty($data['birthday']) ? Carbon::createFromFormat('m/d/Y', $data['birthday']) : null;


            $artist->save();

            $folder = $this->createFolder($artist);
            $artist->folder_id = $folder->id;

            if (!empty($data['genre'])) {
                $genres = explode(', ', $data['genre']);

                $genre_ids = [];
                foreach ($genres as $genre_name) {
                    $genre = $this->saveGenre(trim($genre_name));
                    $genre_ids[] = $genre->id;
                }

                $artist->genres()->sync($genre_ids);
            }

            //TODO: change to location
//            if (!empty($data['hometown'])) {
//                $hometown = $this->findOrSaveHometown($data['hometown']);
////
//                $artist->venue_id = $hometown->id;
//            }

            $artist->save();

            $this->info('Synced: ' . $artist->name . ' (' . $artist->id . ')');
//            }
        }
    }

    public function saveGenre($genre_name)
    {
        if (!empty($genre_name)) {
            $genre = Genre::whereTranslation('name', $genre_name)->first();

            if (!empty($genre)) {
                return $genre;
            }

            $genre = new Genre;
            $genre->name = $genre_name;
            $genre->save();

            return $genre;
        }

        return false;
    }

//    public function findOrSaveHometown($hometown)
//    {
//        $country = Country::where('full_name', 'LIKE', '%' . $hometown . '%')->first();
//
//        $city = City::whereTranslation('name', $hometown)->first();
//
//        if (empty($city)) {
//            $city = new City;
//            $city->name = !empty($hometown) ? $hometown : null;
////            $city->country_id = !empty($country) ? $country->id : null;
//
//            $city->save();
//        }
//
//        return $city;
//    }

    public function createFolder($entity)
    {
        if (empty($entity->folder_id)) {
            $folder = new Folder;
            $folder->path = 'artists/' . $entity->slug;

            $folder->save();

            return $folder;
        }

        return $entity->folder;
    }
}