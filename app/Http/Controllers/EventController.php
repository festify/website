<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function show($slug = null)
    {
        $event = Event::whereTranslation('slug', $slug)->first();
//        $spotlight = $event->files->where('pivot.type', 'spotlight')->first();
//        $logo = $event->files->where('pivot.type', 'logo')->first();
//        $active_files = $event->activeFiles;

        return view('events.details', ['event' => $event, 'spotlight' => $event->spotlight, 'logo' => $event->logo]);
    }

    public function lineup($slug = null)
    {
        $event = Event::whereTranslation('slug', $slug)->first();
        $spotlight = $event->files->where('pivot.type', 'spotlight')->first();
        $logo = $event->files->where('pivot.type', 'logo')->first();
//        dd($event);

        return view('events.performances', ['event' => $event, 'spotlight' => $spotlight, 'logo' => $logo]);
    }
}
