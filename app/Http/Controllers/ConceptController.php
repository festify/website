<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;

class ConceptController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function show($slug = null)
    {
        $event = Event::whereTranslation('slug', $slug)->first();
        $spotlight = $event->files->where('pivot.type', 'spotlight')->first();
        $logo = $event->files->where('pivot.type', 'logo')->first();
//        dd($event);

        return view('events.layout', ['event' => $event, 'spotlight' => $spotlight, 'logo' => $logo]);

//        dd($slug);

//        $id = PageSettings::get('product_id');
//        $lang = locale();
//
//        if(empty($id)) {
//            $product = Product::where('id', '=', $slug)->first();
//        }
//        else {
//            $product = Product::where('id', '=', $id)->first();
//        }
//
//        if (empty($product)) {
//            abort(404);
//        }
//
//        return view('product' . (empty($id) ? 's' : '') . '.show.' . PageSettings::getDisplayView(locale(), 'show'), compact('product', 'lang'));
    }
}
