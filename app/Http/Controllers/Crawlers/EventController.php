<?php namespace App\Http\Controllers\Crawlers;

use App\Models\Organizer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Date;
use App\Models\Venue;
use App\Models\Location;
use App\Models\City;
use App\Models\Country;
use App\Http\Controllers\Controller;
use Spatie\Fractal\FractalFacade as Fractal;
use App\Transformers\EventTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class EventController extends Controller
{
    private $include;
    private $soldOutTerms = [
        ' (SOLD OUT)',
        ' (Sold out)',
        ' - SOLD OUT',
        ' [SOLD OUT]',
        'SOLD OUT - ',
        'Sold out - ',
        ' [Sold Out]',
        ' - sold out -',
        ' (SOLD-OUT)',
        ' - Sold out'
    ];

    public function __construct(Request $request)
    {
//        $this->include = explode(',', Input::get('include'));

//        $accept_language = strtolower($request->headers->get('accept-language'));
//        if ($accept_language) {
//            app()->setLocale($accept_language);
//        }
    }

    public function index(Request $request)
    {
        $organizers = Organizer::whereNotNull('fb_event_page')->where('syncable', 1)->get();
        $organizers = $organizers->pluck('fb_event_page');
//        dd($organizers);

        $client = new Client(); //GuzzleHttp\Client

        foreach ($organizers as $key => $organizer) {
            $response = $client->get('https://graph.facebook.com/v2.12/' . $organizer . '/events?access_token=EAACEdEose0cBALfrcZBYfYWMkcNyASmqAB8leDV511NX0n5dBb2QCGYQxMhSNuP6waNWRRGp9cgCZAy56CjuCNF7nRUn93PFiK11QItMSGLDLlMpRponRhNy3uCZCL0SJMYGPxXBkAtl9456FhGO8z9mEoVuQpU3saLuHGF4i6hAy5nTlxPNkYG0ZATcNhHjWTxkwOTBrwZDZD&debug=all&format=json&method=get&pretty=0&suppress_http_code=1');
            $array = json_decode($response->getBody()->getContents(), true);

//        dd($array);

            foreach ($array['data'] as $event) {
                $this->saveEvent($event);
    //            dd($event);
            }
        }

        dd($array);
    }

    public function saveEvent($data)
    {
//        dd($data);
        $eventName = str_replace($this->soldOutTerms, '', $data['name']);

        $event = Event::whereTranslation('name', $eventName)->first();

        if (empty($event)) {
            $event = new Event;

            $event->name = $eventName;
            $event->description = !empty($data['description']) ? $data['description'] : '';
            $event->fb_id = $data['id'];

            $event->save();

            //Dates
            $event_date = new Date;
            $event_date->starts_at = !empty($data['start_time']) ? Carbon::createFromFormat('Y-m-d\TH:i:sP', $data['start_time']) : null;
            $event_date->ends_at = !empty($data['end_time']) ? Carbon::createFromFormat('Y-m-d\TH:i:sP', $data['end_time']) : null;
            //        $event_date->ends_at = $data['end_time'];
            $event_date->event_id = $event->id;

            $event_date->save();

            //Venue
            if (!empty($data['place'])) {
                $event_venue = Venue::whereTranslation('name', $data['place']['name'])->first();

                if (empty($event_venue)) {
                    $event_venue = new Venue;
                    $event_venue->name = !empty($data['place']['name']) ? $data['place']['name'] : null;
                    $event_venue->fb_id = !empty($data['place']['id']) ? $data['place']['id'] : null;

                    //            $event_venue->event_id = $event->id;

                    //Location
                    if (!empty($data['place']['location'])) {
                        $event_location = Location::where('latitude', $data['place']['location']['latitude'])
                            ->where('longitude', $data['place']['location']['longitude'])
                            ->first();

                        if (empty($event_location)) {
                            if (!empty($data['place']['location']['country'])) {
                                $country = Country::where('name', 'LIKE', $data['place']['location']['country'])->first();
                            } else {
                                $country = null;
                            }

                            $event_location = new Location;
                            $event_location->street_name = !empty($data['place']['location']['street']) ? $data['place']['location']['street'] : null;
                            $event_location->postal_area = !empty($data['place']['location']['zip']) ? $data['place']['location']['zip'] : null;
                            $event_location->latitude = !empty($data['place']['location']['latitude']) ? $data['place']['location']['latitude'] : null;
                            $event_location->longitude = !empty($data['place']['location']['longitude']) ? $data['place']['location']['longitude'] : null;
                            $event_location->country_id = !empty($country) ? $country->id : null;

                            if (!empty($data['place']['location']['city'])) {
                                $event_city = $this->findOrSaveCity($data['place']['location']['city'], $country);

                                $event_location->city_id = !empty($event_city) ? $event_city->id : null;
                            }

                            $event_location->save();
                        }

                        $event_venue->location_id = $event_location->id;
                    }

                    $event_venue->save();
                }

                $event->venue_id = $event_venue->id;
            }

            $event->save();
        }
    }

    public function findOrSaveCity($city, $country = null)
    {
        $event_city = City::whereTranslation('name', $city)->first();

        if (empty($event_city)) {
            $event_city = new City;
            $event_city->name = !empty($city) ? $city : null;
            $event_city->country_id = !empty($country) ? $country->id : null;

            $event_city->save();
        }

        return $event_city;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
//        $event = new Event;
//
//        $event->name = $request->name;
//        $event->description = $request->description;
//
//        $event->save();
//
//        if (!empty($request->dates)) {
//            foreach ($request->dates AS $key => $date) {
//                $event_date[$key] = new Date;
//
//                $event_date[$key]->name = $date['name'];
//                $event_date[$key]->starts_at = $date['starts_at'];
//                $event_date[$key]->ends_at = $date['ends_at'];
//                $event_date[$key]->event_id = $event->id;
//
//                $event_date[$key]->save();
//            }
//        }
//
////        if (!empty($request->venue)) {
////
////        }
//
//        return Fractal::create()
//            ->item($event, new EventTransformer())
//            ->serializeWith(new \League\Fractal\Serializer\JsonApiSerializer())
////            ->parseIncludes($this->include)
//            ->withResourceName('events')
//            ->respond();
    }

    public function show(Event $event)
    {
//        dd($event);
//        if (empty($event) || !$event->exists) {
//            return response()->json([
//                'title' => 'Event not found',
//                'status' => 404,
//            ], 404);
//        }
//        return $this->transformer->item($event, new EventTransformer(), 'event');
    }

}
