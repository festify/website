<?php namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Genre;
use App\Http\Controllers\Controller;
use Spatie\Fractal\FractalFacade as Fractal;
use App\Transformers\GenreTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class GenreController extends Controller
{
    private $include;

    public function __construct(Request $request)
    {
        $accept_language = strtolower($request->headers->get('accept-language'));
        if ($accept_language) {
            app()->setLocale($accept_language);
        }
    }

    public function index(Request $request)
    {
//        $filters    = $request->input('filter');
        $cursor     = $request->get('cursor');
        $limit      = $request->get('limit');
        $fields     = $request->get('fields');

        $paginator = Genre::select('genres.*');

//        if (!empty($filters)) {
//            $filters = VenueFilters::hydrate($request->input('filter', []));
//            $paginator->filterBy($filters);
//        }

        $paginator->join('genre_translations', 'genres.id', '=', 'genre_translations.genre_id')
            ->orderBy('genre_translations.name', 'ASC');


        $paginator = $paginator->skip($cursor)->paginate($limit);

        $genres = $paginator->getCollection();

        $responds = Fractal::create()
            ->collection($genres, new GenreTransformer())
            ->serializeWith(new \League\Fractal\Serializer\JsonApiSerializer())
            ->withResourceName('genres')
            ->paginateWith(new IlluminatePaginatorAdapter($paginator));

        if (!empty($fields)) {
            $responds->parseFieldsets($fields);
        }

        return $responds->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

//    /**
//     * Store a newly created resource in storage.
//     *
//     * @return Response
//     */
//    public function store(Request $request)
//    {
//    }

    public function show(Genre $genre)
    {
        dd($genre);
    }

}
