<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Venue;
use App\Http\Controllers\Controller;
use Spatie\Fractal\FractalFacade as Fractal;
use App\Transformers\VenueTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\QueryFilters\VenueFilters;

class VenueController extends Controller
{
    private $include;

    public function __construct(Request $request)
    {
        $accept_language = strtolower($request->headers->get('accept-language'));
        if ($accept_language) {
            app()->setLocale($accept_language);
        }
    }

    public function index(Request $request)
    {
        $filters    = $request->input('filter');
        $cursor     = $request->get('cursor');
        $limit      = $request->get('limit');
        $fields     = $request->get('fields');

        $paginator = Venue::select('venues.*');

        if (!empty($filters)) {
            $filters = VenueFilters::hydrate($filters);
            $paginator->filterBy($filters);
        }

        $paginator->join('venue_translations', 'venues.id', '=', 'venue_translations.venue_id')
            ->orderBy('venue_translations.name', 'ASC');

        $paginator = $paginator->skip($cursor)->paginate($limit);

        $venues = $paginator->getCollection();

        $responds = Fractal::create()
            ->collection($venues, new VenueTransformer())
            ->serializeWith(new \League\Fractal\Serializer\JsonApiSerializer())
            ->withResourceName('venues')
            ->paginateWith(new IlluminatePaginatorAdapter($paginator));

        if (!empty($fields)) {
            $responds->parseFieldsets($fields);
        }

        return $responds->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

//    /**
//     * Store a newly created resource in storage.
//     *
//     * @return Response
//     */
//    public function store(Request $request)
//    {
//    }

    public function show(Genre $genre)
    {
        dd($genre);
    }

}
