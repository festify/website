<?php namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Date;
use App\Http\Controllers\Controller;
use Spatie\Fractal\FractalFacade as Fractal;
use App\Transformers\EventTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\QueryFilters\EventFilters;


class EventController extends Controller
{
    private $include;

    public function __construct(Request $request)
    {
//        $this->include = explode(',', Input::get('include'));

        $accept_language = strtolower($request->headers->get('accept-language'));
        if ($accept_language) {
            app()->setLocale($accept_language);
        }
    }

    public function index(Request $request)
    {
        $filters    = $request->input('filter');
        $cursor     = $request->get('cursor');
        $limit      = $request->get('limit');
        $fields     = $request->get('fields');

        $paginator = Event::select('events.*');

        if (!empty($filters)) {
            $filters = EventFilters::hydrate($request->input('filter', []));
            $paginator->filterBy($filters);
        }

        $paginator->join('dates', 'events.id', '=', 'dates.event_id')
            ->orderBy('dates.starts_at', 'ASC');

//        //TODO: whereHas?
//        if (!empty($filters['genres']) && str_contains($filters['genres'], ',')) {
//            $genres = explode(',', $filters['genres']);
//            foreach ($genres AS $column => $genre) {
//                $paginator->genres()->orWhere('name', $genre);
//            }
//        } elseif (!empty($filters['genres']) && !str_contains($filters['genres'], ',')) {
//            $paginator->genres()->whereTranslation('name', $filters['genres']);
//        }

//        dd($filters);

//        unset($filters['starts_at']);
//        unset($filters['ends_at']);
//        unset($filters['genres']);
//
//        if (!empty($filters) && is_array($filters)) {
//            foreach ($filters AS $column => $value) {
//                if (!empty($column) && !empty($value)) {
//                    $paginator = $paginator->whereTranslationLike($column, '%' . $value . '%');
//                }
//            }
//        }

//        dd($paginator->toSql());

        $paginator = $paginator->skip($cursor)->paginate($limit);

        $events = $paginator->getCollection();

//        foreach ($events as $event) {
//            dump($event->dates);
//        }

//        dd($events);
        $responds = Fractal::create()
            ->collection($events, new EventTransformer())
            ->serializeWith(new \League\Fractal\Serializer\JsonApiSerializer())
            ->withResourceName('events')
            ->paginateWith(new IlluminatePaginatorAdapter($paginator));

        if (!empty($fields)) {
            $responds->parseFieldsets($fields);
        }

        return $responds->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $event = new Event;

        $event->name = $request->name;
        $event->description = $request->description;

        $event->save();

        if (!empty($request->dates)) {
            foreach ($request->dates AS $key => $date) {
                $event_date[$key] = new Date;

                $event_date[$key]->name = $date['name'];
                $event_date[$key]->starts_at = $date['starts_at'];
                $event_date[$key]->ends_at = $date['ends_at'];
                $event_date[$key]->event_id = $event->id;

                $event_date[$key]->save();
            }
        }

//        if (!empty($request->venue)) {
//
//        }

        return Fractal::create()
            ->item($event, new EventTransformer())
            ->serializeWith(new \League\Fractal\Serializer\JsonApiSerializer())
//            ->parseIncludes($this->include)
            ->withResourceName('events')
            ->respond();
    }

    public function show(Event $event)
    {
        dd($event);
        if (empty($event) || !$event->exists) {
            return response()->json([
                'title' => 'Event not found',
                'status' => 404,
            ], 404);
        }
        return $this->transformer->item($event, new EventTransformer(), 'event');
    }

}
