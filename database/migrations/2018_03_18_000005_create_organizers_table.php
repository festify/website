<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('fb_event_page')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('organizer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();

            $table->integer('organizer_id')->unsigned();
            $table->string('locale')->index();

            $table->unique(['organizer_id', 'locale']);
            $table->foreign('organizer_id')->references('id')->on('organizers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organizers');
        Schema::drop('organizer_translations');
    }
}
