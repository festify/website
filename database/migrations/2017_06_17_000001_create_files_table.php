<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('original_name');
            $table->string('file_name');
            $table->integer('folder_id')->unsigned()->nullable();
            $table->string('mime_type');
            $table->string('extension');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('filables', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('file_id')->unsigned();
            $table->integer('filable_id')->unsigned();
            $table->string('filable_type');
            $table->string('type')->nullable();
            $table->integer('order')->nullable();

            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
        Schema::dropIfExists('filables');
    }
}
