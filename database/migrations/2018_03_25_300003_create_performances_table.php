<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performances', function (Blueprint $table) {
            $table->increments('id');

//            $table->integer('stage_id')->unsigned()->nullable();
            $table->integer('artist_id')->unsigned()->nullable();
            $table->integer('event_id')->unsigned();
            $table->integer('date_id')->unsigned()->nullable();

            $table->timestamp('starts_at')->nullable();
            $table->timestamp('ends_at')->nullable();

//            $table->foreign('stage_id')->references('id')->on('stages')->onDelete('set null');
            $table->foreign('artist_id')->references('id')->on('artists')->onDelete('no action');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('no action');
            $table->foreign('date_id')->references('id')->on('dates')->onDelete('no action');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('performances');
    }
}
