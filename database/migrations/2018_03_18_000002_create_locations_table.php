<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('street_number')->nullable();
            $table->string('street_number_suffix')->nullable();
            $table->string('street_name')->nullable();
            $table->string('street_direction', 2)->nullable();
            $table->string('postal_area')->nullable();

            $table->integer('fb_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

//        Schema::create('location_translations', function (Blueprint $table) {
//            $table->increments('id');
//
//            $table->string('name');
//
////            $table->integer('governing_district_id')->unsigned()->nullable()
//
////            $table->foreign('governing_district_id')->references('id')->on('locations__governingdistricts')->onDelete('cascade');
//
//            $table->timestamps();
//            $table->softDeletes();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
//        Schema::drop('location_translations');
    }
}
